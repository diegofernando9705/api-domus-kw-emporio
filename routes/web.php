<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Pagination\Paginator;
use App\Http\Controllers\InmueblesController;
use DB as DBS;
use Illuminate\Support\Facades\Crypt;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{page?}', function () {
	
	$key = DBS::table('domus_key')->get();

	$valor_key = Crypt::decryptString($key[0]->key);

	$rand = rand(1, 30);
	
	if(isset($_GET['paginate'])){
		$ch_pay = curl_init('http://api.domus.la/pro-grid?page='.$_GET['paginate']);
	}else{
		$ch_pay = curl_init('http://api.domus.la/pro-grid?page='.$rand);
	}

	curl_setopt($ch_pay, CURLOPT_CUSTOMREQUEST, "GET");

	curl_setopt($ch_pay, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch_pay , CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'Authorization: '. $valor_key,
		'perpage: 32'
	)); 
	
	$result_pay = curl_exec($ch_pay);
	
	curl_close($ch_pay);

	$data_pay = json_decode($result_pay, true);


	$total_paginas = $data_pay['total'];
	$paginas = ceil($total_paginas/50);


	/* tipo de inmuebles */

		$tipo = curl_init();

		curl_setopt_array($tipo, array(
		  CURLOPT_URL => "http://api.domus.la/types",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		  	'Content-Type: application/json',
			'Authorization: '. $valor_key
		  ),
		));

		$tipo_pay = curl_exec($tipo);

		curl_close($tipo);

		$tipo_inmuebles = json_decode($tipo_pay, true);

	/* fin Tipo de inmuebles*/



	/* gestion de inmuebles */
		
		$gestion = curl_init();

		curl_setopt_array($gestion, array(
		  CURLOPT_URL => "http://api.domus.la/biz",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    'Content-Type: application/json',
			'Authorization: '. $valor_key,
		  ),
		));

		$response_gestion = curl_exec($gestion);
		$err = curl_error($gestion);

		curl_close($gestion);

		$gestion_inmuebles = json_decode($response_gestion, true);

	/* fin gestion de inmuebles */


	/* ciudades */

		$ciudad = curl_init();

		curl_setopt_array($ciudad, array(
		  CURLOPT_URL => "http://api.domus.la/cities",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		  	'Content-Type: application/json',
			'Authorization: '. $valor_key
		  ),
		));

		$ciudad_pay = curl_exec($ciudad);

		curl_close($ciudad);

		$ciudades = json_decode($ciudad_pay, true);

	/* fin ciudades */


	/* zonas */

		$zona = curl_init();

		curl_setopt_array($zona, array(
		  CURLOPT_URL => "http://api.domus.la/zones",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		  	'Content-Type: application/json',
			'Authorization: '. $valor_key
		  ),
		));

		$zona_pay = curl_exec($zona);

		curl_close($zona);

		$zonas = json_decode($zona_pay, true);

	/* fin zonas */
	
    return view('inmuebles-general', compact('data_pay', 'paginas', 'gestion_inmuebles', 'tipo_inmuebles', 'ciudades', 'zonas'));
});

Route::get('/resultado/{pagina?}', function($page){

	$key = DBS::table('domus_key')->get();
	$valor_key = Crypt::decryptString($key[0]->key);

	$tipo = "type=".$page;


        $url = "http://api.domus.la/pro-grid?".$tipo;
        
        $curl = curl_init();

        $ch_pay = curl_init($url);

        curl_setopt($ch_pay, CURLOPT_CUSTOMREQUEST, "GET");

        curl_setopt($ch_pay, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch_pay , CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: '. $valor_key,
            'perpage: 32'
        )); 

        $result_pay = curl_exec($ch_pay);
        
        curl_close($ch_pay);

        $data_pay = json_decode($result_pay, true);

        $paginas = 0;



	/* tipo de inmuebles */

		$tipo = curl_init();

		curl_setopt_array($tipo, array(
		  CURLOPT_URL => "http://api.domus.la/types",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		  	'Content-Type: application/json',
			'Authorization: '. $valor_key
		  ),
		));

		$tipo_pay = curl_exec($tipo);

		curl_close($tipo);

		$tipo_inmuebles = json_decode($tipo_pay, true);
	/* fin Tipo de inmuebles*/


	/* gestion de inmuebles */
		
		$gestion = curl_init();

		curl_setopt_array($gestion, array(
		  CURLOPT_URL => "http://api.domus.la/biz",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    'Content-Type: application/json',
			'Authorization: '. $valor_key,
		  ),
		));

		$response_gestion = curl_exec($gestion);
		$err = curl_error($gestion);

		curl_close($gestion);

		$gestion_inmuebles = json_decode($response_gestion, true);
	/* fin gestion de inmuebles */


	/* ciudades */

		$ciudad = curl_init();

		curl_setopt_array($ciudad, array(
		  CURLOPT_URL => "http://api.domus.la/cities",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		  	'Content-Type: application/json',
			'Authorization: '. $valor_key
		  ),
		));

		$ciudad_pay = curl_exec($ciudad);

		curl_close($ciudad);

		$ciudades = json_decode($ciudad_pay, true);
	/* fin ciudades */


	/* zonas */

		$zona = curl_init();

		curl_setopt_array($zona, array(
		  CURLOPT_URL => "http://api.domus.la/zones",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		  	'Content-Type: application/json',
			'Authorization: '. $valor_key
		  ),
		));

		$zona_pay = curl_exec($zona);

		curl_close($zona);

		$zonas = json_decode($zona_pay, true);
	/* fin zonas */
	
    	$paginas = 0;

	return view('inmuebles-general', compact('data_pay', 'paginas', 'gestion_inmuebles', 'tipo_inmuebles', 'ciudades', 'zonas'));        
});

Route::get('/inmueble/{code}', function($id){

	$key = DBS::table('domus_key')->get();
	$valor_key = Crypt::decryptString($key[0]->key);

	$curl = curl_init();

	curl_setopt_array($curl, array(
	    CURLOPT_URL => "http://api.domus.la/pro-detail/".$id,
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_ENCODING => "",
	    CURLOPT_MAXREDIRS => 10,
	    CURLOPT_TIMEOUT => 30,
	    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	    CURLOPT_CUSTOMREQUEST => "GET",
	    CURLOPT_HTTPHEADER => array(
	        "authorization: ".$valor_key,
	    ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	$details = json_decode($response, true);
	

	/* INMUEBLES SIMILARES */

		foreach($details['data'] as $inmueble_details){
			$gestion_unico = $inmueble_details['biz'];
			$zona_unico = $inmueble_details['zone'];
			$valor = $inmueble_details['price'];
			$tipo_inmueble = $inmueble_details['type'];
		}


		/* gestion de inmuebles */
		
			$gestion = curl_init();

			curl_setopt_array($gestion, array(
			  CURLOPT_URL => "http://api.domus.la/biz",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(
			    'Content-Type: application/json',
				'Authorization: '. $valor_key,
			  ),
			));

			$response_gestion = curl_exec($gestion);
			$err = curl_error($gestion);

			curl_close($gestion);

			$gestion_inmuebles = json_decode($response_gestion, true);


			foreach($gestion_inmuebles as $inmueble){
				if($inmueble['biz'] === $gestion_unico){
					$code_gestion = $inmueble['code'];
				}
			}
		/* fin gestion de inmuebles */


		/* Zonas de Inmueble */

			$zona = curl_init();

			curl_setopt_array($zona, array(
			  CURLOPT_URL => "http://api.domus.la/zones",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(
			  	'Content-Type: application/json',
				'Authorization: '. $valor_key
			  ),
			));

			$zona_pay = curl_exec($zona);

			curl_close($zona);

			$zonas = json_decode($zona_pay, true);


			foreach($zonas as $zona_for){
				if($zona_for['zone'] === $zona_unico){
					$zona_gestion = $zona_for['code'];
				}
			}
		/* fin zonas de inmuebles */


		/* Valores inmueble */
			$cantidad = explode("/", $valor);

			if(isset($cantidad[1])){

				$operacion = $cantidad[0]*20/100;

				$price_min = $operacion-$cantidad[0];
				$price_max = $operacion+$cantidad[0];
			
			}else{

				$operacion = $valor*20/100;

				$price_min = $operacion-$valor;
				$price_max = $operacion+$valor;

			}

		/* Fin valores inmuebles */

		/* Tipo de Inmueble */

			$tipo = curl_init();

			curl_setopt_array($tipo, array(
			  CURLOPT_URL => "http://api.domus.la/types",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(
			  	'Content-Type: application/json',
				'Authorization: '. $valor_key
			  ),
			));

			$tipo_pay = curl_exec($tipo);

			curl_close($tipo);

			$tipos = json_decode($tipo_pay, true);


			foreach($tipos as $tipo_for){
				if($tipo_for['type'] === $tipo_inmueble){
					$tipo_inmueble = $tipo_for['code'];
				}
			}
		/* fin tipo de inmuebles */

		$random = rand(1, 2);
		$curl_similares = curl_init();

		curl_setopt_array($curl_similares, array(
		    CURLOPT_URL => "http://api.domus.la/pro-grid?zone=".$zona_gestion."&biz=".$code_gestion."&page=".$random."&pcmin=".$price_min."&pcmax=".$price_max."&type=".$tipo_inmueble."",
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_ENCODING => "",
		    CURLOPT_MAXREDIRS => 10,
		    CURLOPT_TIMEOUT => 30,
		    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		    CURLOPT_CUSTOMREQUEST => "GET",
		    CURLOPT_HTTPHEADER => array(
			    'Content-Type: application/json',
				'Authorization: '. $valor_key,
				'perpage: 32'
		    ),
		));

		$response_similares = curl_exec($curl_similares);
		$err = curl_error($curl_similares);

		curl_close($curl_similares);

		$inmuebles_similares = json_decode($response_similares, true);
    /* FIN INMUEBLES SIMILARES */
    
	return view('inmuebles.details', compact('details', 'inmuebles_similares'));
});

Route::post('/inmuebles/busqueda', [InmueblesController::class, 'busqueda']);
Route::post('/formulario/enviar', [InmueblesController::class, 'formulario']);

Route::get('/inmuebles/barrios/{code_ciudad}', [InmueblesController::class, 'barrios']);

