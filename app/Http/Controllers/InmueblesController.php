<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use DB as DBS;
use Mail; //Importante incluir la clase Mail, que será la encargada del envío
use PHPMailer\PHPMailer;

class InmueblesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function busqueda(Request $request){

        $key = DBS::table('domus_key')->get();
        $valor_key = Crypt::decryptString($key[0]->key);
        
        $estrato = $request->input('estrato');
        $areacons = $request->input('areacons');
        $arealote = $request->input('arealote');
        $habitaciones = $request->input('habitaciones');
        $banos = $request->input('banos');
        $anocons = $request->input('anocons');
        $gestion = $request->input('gestion');

        $pricemin = $request->input('pricemin');
        $pricemax = $request->input('pricemax');

        $price_vta_min = $request->input('price_vta_max');
        $price_vta_min = $request->input('price_vta_max');


        $consulta = ['?'];


        if(!empty($request->input('ciudad'))){
            
            $ciudad = "city=".$request->input('ciudad');

            array_push($consulta, $ciudad);
            array_push($consulta, "&");
        }


        if(!empty($request->input('zona'))){
            
            $zone = $request->input('zona');
            $zona = "zone=".$zone;

            array_push($consulta, $zona);
            array_push($consulta, "&");
        }


        if(!empty($request->input('tipo'))){
            
            $type = $request->input('tipo');
            $tipo = "type=".$type;
            
            array_push($consulta, $tipo);
            array_push($consulta, "&");
        }


        if(!empty($request->input('gestion'))){
            
            $gestion = $request->input('gestion');
            $destination = "biz=".$gestion;
            
            array_push($consulta, $destination);
            array_push($consulta, "&");
        }


        if(!empty($request->input('habitaciones'))){
            
            $habitaciones = $request->input('habitaciones');
            $bedrooms = "bedrooms=".$habitaciones;
            
            array_push($consulta, $bedrooms);
            array_push($consulta, "&");
        }


        if(!empty($request->input('banos'))){
            
            $banos = $request->input('banos');
            $bathrooms = "bathrooms=".$banos;
            
            array_push($consulta, $bathrooms);
            array_push($consulta, "&");
        }


        if(!empty($request->input('estrato'))){
            
            $estrato = $request->input('estrato');
            $stratum = "stratum=".$estrato;
            
            array_push($consulta, $stratum);
            array_push($consulta, "&");
        }


        if(!empty($request->input('arealote'))){
            
            $area_lote = $request->input('arealote');
            $arealote = "area_lot=".$area_lote;
            
            array_push($consulta, $arealote);
            array_push($consulta, "&");
        }


        if(!empty($request->input('areacons'))){
            
            $area_const = $request->input('areacons');
            $areacons = "area_cons=".$area_const;
            
            array_push($consulta, $areacons);
            array_push($consulta, "&");
        }


        if(!empty($request->input('barrio'))){
            
            $barrios = $request->input('barrio');
            $barrio = "neighborhood_id=".$barrios;
            
            array_push($consulta, $barrio);
            array_push($consulta, "&");
        }


        if(!empty($request->input('anocons'))){
            
            $ano_cons = $request->input('anocons');
            $anocons = "built_year=".$ano_cons;
            
            array_push($consulta, $anocons);
            array_push($consulta, "&");
        }


        if(!empty($request->input('pricemin'))){
            
            $pcmin = "pcmin=".$request->input('pricemin');

            array_push($consulta, $pcmin);
            array_push($consulta, "&");
        }


        if(!empty($request->input('pricemax'))){
            
            $pcmax = "pcmax=".$request->input('pricemax');

            array_push($consulta, $pcmax);
            array_push($consulta, "&");
        }


        if(!empty($request->input('price_vta_min'))){
            
            $pvmin = "pvmin=".$request->input('price_vta_min');

            array_push($consulta, $pvmin);
            array_push($consulta, "&");
        }


        if(!empty($request->input('price_vta_max'))){
            
            $pvmax = "pvmax=".$request->input('price_vta_max');

            array_push($consulta, $pvmax);
            array_push($consulta, "&");
        }


        $cantidad = count($consulta);
        $valor = $cantidad-1;
        unset($consulta[$valor]);
        $get = implode($consulta, "");


        $url = "http://api.domus.la/pro-grid".$get;

        $curl = curl_init();

        $ch_pay = curl_init($url);

        curl_setopt($ch_pay, CURLOPT_CUSTOMREQUEST, "GET");

        curl_setopt($ch_pay, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch_pay , CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: '. $valor_key,
            'perpage: 50'
        )); 

        $result_pay = curl_exec($ch_pay);
        
        curl_close($ch_pay);

        $data_pay = json_decode($result_pay, true);
        $paginas = 0;

        if($data_pay['total'] <= 0){
          return view('conf.formulario');
        }else{
          return view('conf.inmuebles-busqueda', compact('data_pay', 'url', 'paginas'));
        }

    }

    public function barrios($code_ciudad){

        $key = DBS::table('domus_key')->get();
        $valor_key = Crypt::decryptString($key[0]->key);

        /* barrios */

            $barrio = curl_init();

            curl_setopt_array($barrio, array(
              CURLOPT_URL => "http://api.domus.la/neighborhoods?city=".$code_ciudad,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: '. $valor_key
              ),
            ));

            $barrio_pay = curl_exec($barrio);

            curl_close($barrio);
            
            $pagination = 0;
            $barrios = json_decode($barrio_pay, true);
        /* fin barrios */

        return view('conf.barrios', compact('barrios', 'pagination'));
    }

    public function formulario(Request $request){
        
        $pass = 'Uuc,WIB$C!0C';

        $text             = 'Se ha recibido un mensaje de la aplicacion: inmuebles.kwemporio.co <br><b>Nombre:</b>'.$request->input('nombre').'<br><b>Celular:</b>'.$request->input('celular').'<br><b>Correo:</b>'.$request->input('correo').'<br><b>Tipo de inmueble:</b>'.$request->input('tipo').'';
        $mail             = new PHPMailer\PHPMailer(); // create a n
        $mail->isSMTP();
        $mail->SMTPDebug  = 1; // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth   = true; // authentication enabled
        $mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for Gmail
        $mail->Host       = "marketinginmobiliario3d.com";
        $mail->Port       = 587; // or 587
        $mail->IsHTML(true);
        $mail->Username = "desarrollo@marketinginmobiliario3d.com";
        $mail->Password = $pass;
        $mail->SetFrom("desarrollo@marketinginmobiliario3d.com", 'Inmuebles KW Emporio');
        $mail->Subject = "Una persona se ha comunicado";
        $mail->Body    = $text;
        $mail->AddAddress("kwemporio@kwemporio.co");
        $mail->AddAddress("diegofernando9705@gmail.com");

        if ($mail->Send()) {
            return 1;
        } else {
            return false;
        }


    }
}
