@extends('layouts.app')


@section('carousel')
	@include('conf.carousel')
@endsection

@section('content')

		<div class="campo_busqueda col-12 com-sm-12 col-md-12 col-xl-12 col-lg-12">
			@include('conf.panel_busqueda')
		</div>
		
		<div class="titulo_resultados col-12 com-sm-12 col-md-12 col-xl-12 col-lg-12">
			<p>Resultados de la Búsqueda:</p>
			<hr>
		</div>
		
		<div class="contenedor-resultado col-12 com-sm-12 col-md-12 col-xl-12 col-lg-12" id="contenedor-resultado">
			<div class="row">
				@foreach($data_pay['data'] as $inmueble)
					<div class="inmuebles-unico col-12 col-sm-3 col-md-6 col-xl-3 col-lg-3">
						<div class="div-inmueble card">
							@if(empty($inmueble['image']))
							<div class="header-card" style="background-image: url('https://media-exp1.licdn.com/dms/image/C4E1BAQGBu6Mc9ulUgg/company-background_10000/0?e=2159024400&v=beta&t=coYBxVpL7xK9cQnl3E_ctHbcIo9PQXCkz0R_xq8dgOg'); height: 200px; background-size: cover; background-repeat: no-repeat;">						
							</div>
							@else
							<div class="header-card" style="background-image: url('{{ $inmueble['image'] }}'); height: 200px; background-size: cover; background-repeat: no-repeat;">						
							</div>
							@endif
							
							<div class="card-body">
								<div class="price">
									@if($inmueble['biz'] == 'VENTA')
										Precio de venta: $ {{ number_format($inmueble['saleprice']) }}
									@elseif($inmueble['biz'] == 'ARRIENDO')
										Precio de alquiler: $ {{ number_format($inmueble['rent']) }}
									@else
										<p></p>
									@endif
								</div>
								<button class="btn btn-primary btn-sm">{{ $inmueble['type'] }}</button>
								<button class="btn btn-danger btn-sm">{{ $inmueble['biz'] }}</button>
								<br><br>
								<h5 class="card-title">{{ $inmueble['neighborhood'] }}</h5>
								<p class="card-text descripcion-texto" align="justify">{{ substr($inmueble['description'], 0, 100) }} (...)</p>
								<center>
									<a href="{{ url('inmueble', $inmueble['codpro']) }}" class="btn btn-danger">Ver inmueble</a>
								</center>
							</div>
							<div class="card-footer" >
								<div class="caracteristicas-inmueble col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div class="row">
										<div class="banos col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
											<i class="fas fa-bath"></i> {{ $inmueble['bathrooms'] }}
										</div>
										<div class="habitaciones col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
											<i class="fas fa-bed"></i> {{ $inmueble['bedrooms'] }}
										</div>
										<div class="garaje col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
											<i class="fas fa-warehouse"></i> {{ $inmueble['parking'] }}
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				@endforeach
				@include('conf.pagination')
			</div>
		</div>

		

@endsection