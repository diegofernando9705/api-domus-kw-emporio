<!DOCTYPE html>
<html>
<head>
	<title>Inmuebles KW Emporio</title>
	<meta name="viewport" content="width=device-width, user-scalable=no">

	<link rel="icon" href="https://fincaraiz.elpais.com.co/uploads/minisites/logo/medium_1509482861-7443.jpg">
	
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<link href="{{ asset('css/style.css') }}" rel="stylesheet" />


	<script src="https://kit.fontawesome.com/edaf9c69c8.js" crossorigin="anonymous"></script>
	<style>

	body{
		overflow-x: hidden;
	}

	ul.pagination {
	    display: inline-block;
	    padding: 0;
	    margin: 0;
	}

	i{
		color: #e51937 !important
	}
	ul.pagination li {display: inline;}

	ul.pagination li a {
	    color: black;
	    float: left;
	    padding: 8px 16px;
	    text-decoration: none;
	}

	.panel-busqueda{
		border-radius: 10px;
		margin-top: 30px;
		margin-bottom: 30px;
		padding-top: 20px;
		padding-bottom: 40px;
		padding-left: 20px;
		padding-right: 20px;
		box-shadow: 1px 1px 15px 1px rgb(0 0 0 / 50%);
	}

	.titulo_resultados p{
		font-size: 30px;
		font-weight: bold;
	}

	.div-inmueble{
		border-radius: 10px;
		box-shadow: 1px 1px 15px 1px rgb(0 0 0 / 50%);
	}

	.card-footer{
		text-align: center;
	}

	.price{
		padding-left: 10px;
		padding-right: 10px;
		padding-top: 6px;
		padding-bottom: 6px;
		position: absolute;
		z-index: 999;
		background-color: #b02a37;
		top: 10px;
		left: 10px;
		color: white;
		border-radius: 5px;
		box-shadow: 1px 1px 15px 1px rgb(0 0 0 / 50%);
	}
	</style>

</head>

<body>

	<header>

		<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark"  style="background-color: black !important;">
		  <a class="navbar-brand" href="{{ url('') }}" style="color:white !important;">
		  	<img src="https://kwemporio.co/wp-content/uploads/2020/06/Fondo-Negro2.png" width="100%">
		  </a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" style="color:white !important;">
		    <span class="navbar-toggler-icon" style="color:white !important;"></span>
		  </button>

		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav ml-auto">
	          <li class="nav-item active">
	            <a class="nav-link" aria-current="page" href="https://kwemporio.co/" style="color:white !important;" target="_blank">Inicio</a>
	          </li>
	          <li class="nav-item">
	            <a class="nav-link" href="https://www.kwcolombia.com/KWEmporio" style="color:white !important;" target="_blank">Agentes</a>
	          </li>
	          <li class="nav-item">
	            <a class="nav-link" href="https://kwemporio.co/servicios/" style="color:white !important;" target="_blank">Servicios</a>
	          </li>
	          <li class="nav-item">
	            <a class="nav-link" href="https://kwemporio.co/oficinas/" style="color:white !important;" target="_blank">Oficinas</a>
	          </li>
	          <li class="nav-item">
	            <a class="nav-link" href="https://kwemporio.co/oficinas/" style="color:white !important;" target="_blank">Consejos</a>
	          </li>
	          <li class="nav-item">
	            <a class="nav-link" href="https://kwemporio.co/contacto/" style="color:white !important;" target="_blank">Contacto</a>
	          </li>
		    </ul>

		  </div>
		</nav>
	</header>

	@yield('carousel')

	<div class="row contenedor-principal" style="padding-right: 8%; padding-left: 8%;">
		@yield('content')
	</div>
	
	<div class="contenedor-whatsapp">
		<a href="https://web.whatsapp.com/send?phone=573166183100" target="_blank">
			<label class="label-whatsapp">¿Necesitas más información?</label>
		</a>
		<a href="https://web.whatsapp.com/send?phone=573166183100" target="_blank">
			<img src="{{ asset('images/whatsapp.png') }}">
		</a>
	</div>
</body>
	

  <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	<script type="text/javascript">

		$(document).ready(function() {
		    $('.selector').select2();
		});

		$( document ).ready(function(){
		    $('.carousel').carousel({
		      interval: 2000
		    })
		});

		$(document).on("click", "#btn-busqueda", function(){

			var formData = $("#formulario-busqueda").serialize();
			
			$.ajax({
				headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
	                type: "POST",
	                url: "/inmuebles/busqueda", 
	                data: formData,
	                success: function (data) {
	                	console.log(data);
	                	$("#contenedor-resultado").html(data);
	               }
	            });
		});


		$(document).on("change", "#ciudad", function(){
			
			var code_ciudad = $(this).val();

			$.ajax({
	                type: "get",
	                url: "/inmuebles/barrios/"+code_ciudad,
	                success: function (data) {
	                	$("#barrios").html(data);
	               }
	            });
		});


		/* abrir busqueda avanzada */
			$(document).on("click", "#busqueda-avanzada", function(){

				if($(this).attr('data-op') == 'abrir'){
					
					$("#div-busqueda-avanzada").attr("style","margin-top: 15px; overflow: hidden; height:220px; transition:1s;");
					$("#busqueda-avanzada").html('Cerrar búsqueda avanzada');

					$(this).attr('data-op', 'cerrar');

				}else{
					
					$("#div-busqueda-avanzada").attr("style","margin-top: 15px; overflow: hidden; height:0px; transition:1s;");
					$("#busqueda-avanzada").html('Búsqueda avanzada');

					$(this).attr('data-op', 'abrir');

				}
			});
		/* fin abrir busqueda avanzada */



		$(document).on("keyup", ".numero", function(){
			console.log("Hola");
		  // skip for arrow keys
		  if(event.which >= 37 && event.which <= 40){
		    event.preventDefault();
		  }

		  $(this).val(function(index, value) {
		    return value
		      .replace(/\D/g, "")
		      .replace(/([0-9])([0-9]{0})$/, '$1.$2')  
		      .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",")
		    ;
		  });
		});


		$(document).on("click", "#envio-formulario", function(){

			var formData = $("#formulario-comunicacion").serialize();
			
			document.getElementById('nombre').disabled = true;
			document.getElementById('celular').disabled = true;
			document.getElementById('correo').disabled = true;
			document.getElementById('tipo').disabled = true;
			document.getElementById('envio-formulario').disabled = true;

			$.ajax({
				headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
	                type: "POST",
	                url: "/formulario/enviar", 
	                data: formData,
	                success: function (data) {
	                	console.log(data);
	                	if(data == false){
	                		console.log(data);
	                		alert('Hubo un error al enviar el mensaje; Comunicate a: desarrollo@marketinginmobiliario3d.com');
	                	}else{
	                		alert('Mensaje enviado');
	                		setTimeout(function(){ location.reload(); }, 1000);
	                	}
	               }
	            });
		});


	</script>
	
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>


</html>