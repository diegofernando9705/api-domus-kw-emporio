@extends('layouts.app')

@section('carousel')
	<div class="encabezado-inmueble">
		<div class="row titulo-encabezado">
			<div class="container container-encabezado">
				<p>Detalles del Inmueble</p>
			</div>
		</div>
	</div>
@endsection

@section('content')

	<div class="row row-inmueble-detalle">
		@foreach($details['data'] as $inmueble)
			<div class="titulo-inmueble col-12 col-sm-12 col-md-12 col-lg-12">
				<h3>{{ $inmueble['neighborhood'] }}</h3>
				<hr>
			</div>
			<?php $i=0; ?>
			<div class="inmuebles-unico col-12 col-md-8 col-sm-9 col-lg-9 col-xl-9">
				<div class="card">
					<div class="carousel-inner">
						<div id="inmueble" class="carousel slide" data-ride="carousel">
					    	
						    @foreach($inmueble['images'] as $image)
						    	@if(empty($image['imageurl']))
						    		<div class="carousel-item active">							    		
						    			<img class="d-block w-100" src="https://media-exp1.licdn.com/dms/image/C4E1BAQGBu6Mc9ulUgg/company-background_10000/0?e=2159024400&v=beta&t=coYBxVpL7xK9cQnl3E_ctHbcIo9PQXCkz0R_xq8dgOg" alt="First slide">
							    	</div>
						    	@else
							    	<?php $i++; ?>
							    	@if($i==1)
									    <div class="carousel-item active">
									      <img class="d-block w-100" src="{{ $image['imageurl'] }}" alt="First slide">
									    </div>
							    	@else
									    <div class="carousel-item">
									      <img class="d-block w-100" src="{{ $image['imageurl'] }}" alt="First slide">
									    </div>
							    	@endif
							    @endif
						    @endforeach 

					  </div>
					  <a class="carousel-control-prev" href="#inmueble" role="button" data-slide="prev">
					    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
					    <span class="sr-only">Previous</span>
					  </a>
					  <a class="carousel-control-next" href="#inmueble" role="button" data-slide="next">
					    <span class="carousel-control-next-icon" aria-hidden="true"></span>
					    <span class="sr-only">Next</span>
					  </a>
					</div>

					
					<div class="card-body">
						<h5 class="card-title">{{ $inmueble['neighborhood'] }}</h5>
						<p class="card-text" align="justify">{{ $inmueble['description'] }}</p>

						<div class="caracteristicas-inmueble" >
							<h4>Caracteristicas:</h4>
							<hr>
							<div class="row">
								@foreach($inmueble['amenities'] as $amenities)
									<div class="caracteristica-unica col-12 col-sm-3 col-md-4 col-xl-3 col-lg-3">
										<li>{{ $amenities['name'] }}</li>
									</div>
								@endforeach
							</div> 
						</div>
						<center>
							<a href="{{ url('/') }}" class="btn btn-success">Volver</a>
						</center>
					</div>
				</div>
			</div>

			<div class="asesor col-12 col-md-4 col-sm-3 col-lg-3 col-xl-3">
				<div class="ficha_tecnica">
					<div class="encabezado-tecnica">
						<p>Ficha t&eacute;cnica</p>
					</div>

					<div class="informacion-tecnica">
						<div class="imagen-asesor col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
							<button class="btn btn-primary btn-sm">{{ $inmueble['type'] }}</button>
							<button class="btn btn-danger btn-sm">{{ $inmueble['biz'] }}</button>

							<p class="valores_tecnicos">
								@if($inmueble['biz'] == 'VENTA')
									<strong>Precio venta:</strong> ${{ number_format($inmueble['saleprice']) }}
								@elseif($inmueble['biz'] == 'ARRIENDO')
									<strong>Precio alquiler:</strong> ${{ number_format($inmueble['rent']) }}
								@else
									<p></p>
								@endif
								<br>
								<i class="fas fa-map-marker-alt"></i> {{ $inmueble['neighborhood'] }}
							</p>
							
							<div class="otras-caracteristicas col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" >
								<div class="row">
									<div class="fila-uno col-12 col-sm-6 col-md-12 col-lg-6 col-xl-6">
										<i class="fas fa-chart-area"></i>&nbsp;<strong>Area lote:</strong> {{ $inmueble['area_cons'] }}
										<br>
										<i class="fas fa-bath"></i>&nbsp;<strong>Baños:</strong> {{ $inmueble['bathrooms'] }}
										<br>
										<i class="fas fa-warehouse"></i>&nbsp;<strong>Garaje:</strong> {{ $inmueble['parking'] }}
										<br>
										<i class="fas fa-home"></i>&nbsp;<strong>Edad:</strong> {{ $inmueble['bedrooms'] }}
									</div>

									<div class="fila-dos col-12 col-sm-6 col-md-12 col-lg-6 col-xl-6">
										<i class="fas fa-chart-area"></i> <strong>Area privada:</strong> {{ $inmueble['area_lot'] }}
										<br>
										<i class="fas fa-bed"></i> <strong>Alcobas:</strong> {{ $inmueble['bedrooms'] }}
										<br>
										<i class="fas fa-chart-line"></i> <strong>Estrato:</strong> {{ $inmueble['stratum'] }}
										<br>
										<i class="fas fa-user-tie"></i> <strong>Administracion:</strong> {{ $inmueble['administration'] }}
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="encabezado-asesor" >
					<p>Asesor</p>
				</div>

				<div class="informacion-asesor">
					<div class="row">
					@foreach($inmueble['broker'] as $asesor)
						<div class="imagen-asesor col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="text-align: center;">
							@if(empty($asesor['picture']))
								<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTINyH05M3SpS9q2_jAa5BvlJYazlWi9n3eLQ&usqp=CAU">		
							@else
								<img src="{{ $asesor['picture'] }}" >
							@endif	
						</div>
						<div class="personal col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
							<div class="row">
							<table class="table table-hover col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
								<tr>
									<td scope="col"><th>Nombre asesor:</th></td>
									<td scope="col">{{ $asesor['name'] }} {{ $asesor['last_name'] }}</td>
								</tr>
								<tr>
									<td scope="col"><th>Telefono asesor:</th></td>
									<td scope="col">{{ $asesor['telephone'] }}</td>
								</tr>
								<tr>
									<td scope="col"><th>Celular asesor:</th></td>
									<td scope="col">(+57) {{ $asesor['movil_phone'] }}</td>
								</tr>
								<tr>
									<td scope="col"><th>Correo asesor:</th></td>
									<td scope="col">{{ $asesor['email'] }}</td>
								</tr>
							</table>
							</div>
						</div> 
						<div class="comunicaciones col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
							<div class="row">
								<div class="numero col-4 col-md-4 col-sm-4 col-lg-4 col-xl-4">
									<a href="{{ 'https://api.whatsapp.com/send?phone=57'.$asesor['movil_phone'].'&text=Hola 👋 estoy interesado en el inmueble de '.$inmueble['neighborhood'].' el código es: *'. $inmueble['codpro'].'* Muchas gracias!' }}" target="_blank">
										<i class="fab fa-whatsapp" ></i>
									</a>
								</div>
								<div class="numero col-4 col-md-4 col-sm-4 col-lg-4 col-xl-4">
									<a href="mailto:{{ $asesor['email'] }}?subject=Me encuentro interesado en un inmueble&body=Deseo más información sobre el inmueble, el código es {{ $inmueble['codpro'] }}.">
										<i class="far fa-envelope"></i>
									</a>
								</div>
								<div class="numero col-4 col-md-4 col-sm-4 col-lg-4 col-xl-4">
									<a href="tel:{{ $asesor['movil_phone'] }}">
										<i class="fas fa-mobile-alt"></i>
									</a>
								</div>

							</div>
						</div>
					@endforeach
				</div>
				</div>
			</div>
		@endforeach
	</div>
	<div class="row">
		<div class="titulo-similares col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<h3>Inmuebles similares</h3>
			<hr>
		</div>
		
@foreach($inmuebles_similares['data'] as $inmueble)
<div class="inmuebles-unico col-12 col-sm-3 col-md-6 col-xl-3 col-lg-3">
						<div class="div-inmueble card">
							@if(empty($inmueble['image']))
							<div class="header-card" style="background-image: url('https://media-exp1.licdn.com/dms/image/C4E1BAQGBu6Mc9ulUgg/company-background_10000/0?e=2159024400&v=beta&t=coYBxVpL7xK9cQnl3E_ctHbcIo9PQXCkz0R_xq8dgOg'); height: 200px; background-size: cover; background-repeat: no-repeat;">						
							</div>
							@else
							<div class="header-card" style="background-image: url('{{ $inmueble['image'] }}'); height: 200px; background-size: cover; background-repeat: no-repeat;">						
							</div>
							@endif
							
							<div class="card-body">
								<div class="price">
									@if($inmueble['biz'] == 'VENTA')
										Precio de venta: $ {{ number_format($inmueble['saleprice']) }}
									@elseif($inmueble['biz'] == 'ARRIENDO')
										Precio de alquiler: $ {{ number_format($inmueble['rent']) }}
									@else
										<p></p>
									@endif
								</div>
								<button class="btn btn-primary btn-sm">{{ $inmueble['type'] }}</button>
								<button class="btn btn-danger btn-sm">{{ $inmueble['biz'] }}</button>
								<br><br>
								<h5 class="card-title">{{ $inmueble['neighborhood'] }}</h5>
								<p class="card-text" align="justify">{{ substr($inmueble['description'], 0, 100) }} (...)</p>
								<center>
									<a href="{{ url('inmueble', $inmueble['codpro']) }}" class="btn btn-danger">Ver inmueble</a>
								</center>
							</div>
							<div class="card-footer" >
								<div class="caracteristicas-inmueble col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div class="row">
										<div class="banos col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
											<i class="fas fa-bath"></i> {{ $inmueble['bathrooms'] }}
										</div>
										<div class="habitaciones col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
											<i class="fas fa-bed"></i> {{ $inmueble['bedrooms'] }}
										</div>
										<div class="garaje col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
											<i class="fas fa-warehouse"></i> {{ $inmueble['parking'] }}
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					@endforeach
@endsection