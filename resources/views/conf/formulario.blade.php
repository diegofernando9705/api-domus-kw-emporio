<div class="container" style="margin-top: 100px; margin-bottom: 150px;">
	<center>
		<h3>¿No encuentras el inmueble deseado?</h3>
	</center>
	<br>
	<div class="alert alert-danger">
		Llena el formulario y uno de nuestros agentes inmobiliarios lo atenderá lo más pronto posible
	</div>
	<hr>
	<form id="formulario-comunicacion">
		@csrf
		<div class="form-group">
			<label><strong><code>(*)</code>&nbsp; Nombre:</strong></label>
			<input type="text" class="form-control" name="nombre" id="nombre">
		</div>
		<div class="form-group">
			<label><strong><code>(*)</code>&nbsp; Celular:</strong></label>
			<input type="text" class="form-control" name="celular" id="celular">
		</div>
		<div class="form-group">
			<label><strong><code>(*)</code>&nbsp; Correo:</strong></label>
			<input type="email" class="form-control" name="correo" id="correo">
		</div>
		<div class="form-group">
			<label><strong><code>(*)</code>&nbsp; ¿Qu&eacute; tipo de inmueble esta buscando?</strong></label>
			<input type="text" class="form-control" name="tipo" id="tipo">
		</div>
		<center>
			<input type="button" class="btn btn-success" id="envio-formulario" value="Enviar">
		</center>
	</form>
</div>