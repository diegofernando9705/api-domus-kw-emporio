<nav arial-label="Page navigation example">
	<ul class="pagination">
		<li class="page-item">
			@if(isset($_GET['paginate']))
				<a class="page-link" href="?paginate={{ $_GET['paginate']-1 }}">
					Anterior
				</a>
			@else
				<a class="page-link" href="#">
					Anterior
				</a>
			@endif
		</li>
		<?php 
			for($i=0;$i<$paginas;$i++):
		?>
			<li class="page-item @if(isset($_GET['paginate']) && $_GET['paginate'] == $i+1) active @endif">
				<a class="page-link " href="?paginate={{ $i+1 }}" >
					<?php echo $i+1; ?>
				</a>
			</li>
		<?php endfor ?>

		<li class="page-item">
			@if(isset($_GET['paginate']))
				<a class="page-link" href="?paginate={{ $_GET['paginate']+1 }}">
					Siguiente
				</a>
			@else
				<a class="page-link" href="#">
					Siguiente
				</a>
			@endif
		</li>
	</ul>
</nav>
