<div class="row" style="margin-left: 10px;">
	<div class="form-group col-12 col-md-2 col-sm-2 col-lg-2 col-xl-2">
		<label><small><b>Número de habitaciones:</b></small></label>
		<select class="form-control" name="habitaciones">
			<option value="" selected=""></option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			<option value="6">6</option>
			<option value="7">7</option>
			<option value="8">8</option>
			<option value="9">9</option>
			<option value="10">10</option>
		</select>
	</div>

	<div class="form-group col-12 col-md-2 col-sm-2 col-lg-2 col-xl-2">
		<label><small><b>Número de baños:</b></small></label>
		<select class="form-control" name="banos">
			<option value="" selected=""></option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			<option value="6">6</option>
			<option value="7">7</option>
			<option value="8">8</option>
			<option value="9">9</option>
			<option value="10">10</option>
		</select>
	</div>

	<div class="form-group col-12 col-md-2 col-sm-2 col-lg-2 col-xl-2">
		<label><small><b>Estrato:</b></small></label>
		<select class="form-control" name="estrato">
			<option value="" selected=""></option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			<option value="6">6</option>
			<option value="7">7</option>
			<option value="8">8</option>
			<option value="9">9</option>
			<option value="10">10</option>
		</select>
	</div>

	<div class="form-group col-12 col-md-4 col-sm-4 col-lg-4 col-xl-4">
		<label><small><b>Rango valor alquiler:</b></small></label>
		<div class="row">
			<input class="form-control col numero" type="text" name="pricemin" placeholder="Precio minimo" style="margin-right: 2px;">
			<input class="form-control col numero" type="text" name="pricemax" placeholder="Precio maximo">
		</div>
	</div>

	<div class="form-group col-12 col-md-2 col-sm-2 col-lg-2 col-xl-2"></div>

	<div class="form-group col-12 col-md-2 col-sm-2 col-lg-2 col-xl-2" style="margin-top: 15px;">
		<label><small><b>Area lote:</b></small></label>
		<div class="input-group">
		  <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" name="arealote">
		  <div class="input-group-append">
		    <span class="input-group-text">m²</span>
		  </div>
		</div>
	</div>
	
	<div class="form-group col-12 col-md-2 col-sm-2 col-lg-2 col-xl-2" style="margin-top: 15px;">
		<label><small><b>Area construida:</b></small></label>
		<div class="input-group">
		  <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" name="areacons">
		  <div class="input-group-append">
		    <span class="input-group-text">m²</span>
		  </div>
		</div>
	</div>
	
	<div class="form-group col-12 col-md-2 col-sm-2 col-lg-2 col-xl-2" style="margin-top: 15px;">
		<label><small><b>Año de construcción:</b></small></label>
		<div class="input-group">
		  <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" name="anocons">
		  <div class="input-group-append">
		    <span class="input-group-text">año(s)</span>
		  </div>
		</div>
	</div>
	
	<div class="form-group col-12 col-md-4 col-sm-4 col-lg-4 col-xl-4">
		<label><small><b>Rango valor venta:</b></small></label>
		<div class="row">
			<input class="form-control col numero" type="text" name="price_vta_min" placeholder="Precio minimo" style="margin-right: 2px;">
			<input class="form-control col numero" type="text" name="price_vta_max" placeholder="Precio maximo">
		</div>
	</div>
	
</div>