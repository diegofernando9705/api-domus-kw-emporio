<div class="row panel-busqueda">
	<div class="descripcion-panel col-12 col-md-12 col-sm-12 col-lg-12 col-xl-12">
		<h4>Filtro de Búsqueda:</h4>
	</div>
	<hr>
	<form id="formulario-busqueda">
		@csrf
		@method('POST')
		<div class="row">

			<div class="form-group col-12 col-md-3 col-sm-2 col-lg-2 col-xl-2">
				<label><small><b>Gesti&oacute;n de inmuebles:</b></small></label>
				<select class="form-control selector col-12 col-md-12 col-sm-12 col-lg-12 col-xl-12" name="gestion">
					<option value="0" selected="" disabled="">Gestión de Inmuebles</option>
					@foreach($gestion_inmuebles as $inmuebles)
						<option value="{{ $inmuebles['code'] }}"> {{ $inmuebles['biz'] }} </option>
					@endforeach
				</select>
			</div>

			<div class="form-group col-12 col-md-3 col-sm-2 col-lg-2 col-xl-2">
				<label><small><b>Tipo de inmuebles:</b></small></label>
				<select class="form-control selector col-12 col-md-12 col-sm-12 col-lg-12 col-xl-12" name="tipo">
					<option value="0" selected="" disabled="">Tipo de inmueble</option>
					@foreach($tipo_inmuebles as $inmuebles)
						<option value="{{ $inmuebles['code'] }}"> {{ $inmuebles['type'] }} </option>
					@endforeach
				</select>
			</div>

			<div class="form-group col-12 col-md-3 col-sm-2 col-lg-2 col-xl-2">
				<label><small><b>Ciudad:</b></small></label>
				<select class="form-control selector col-12 col-md-12 col-sm-12 col-lg-12 col-xl-12" name="ciudad" id="ciudad">
					<option value="0" selected="" disabled="">Ciudades</option>
					@foreach($ciudades as $inmuebles)
						<option value="{{ $inmuebles['code'] }}"> {{ $inmuebles['city'] }} </option>
					@endforeach
				</select>
			</div>

		<div class="form-group col-12 col-md-3 col-sm-2 col-lg-2 col-xl-2">
			<label><small><b>Zonas:</b></small></label>
			<select class="form-control selector col-12 col-md-12 col-sm-12 col-lg-12 col-xl-12" name="zona">
					<option value="0" selected="" disabled="">Zonas</option>
				@foreach($zonas as $inmuebles)
					<option value="{{ $inmuebles['code'] }}"> {{ $inmuebles['zone'] }} </option>
				@endforeach
			</select>
		</div>

			<div class="form-group col-12 col-md-3 col-sm-2 col-lg-2 col-xl-2">
				<label><small><b>Barrio:</b></small></label>
				<select class="form-control selector col-12 col-md-12 col-sm-12 col-lg-12 col-xl-12" name="barrio" id="barrios">
					<option value="true" selected="" disabled="">Selecciona una ciudad</option>
					
				</select>
			</div>
			
			<div class="form-group col-2 col-md-3 col-sm-2 col-lg-2 col-xl-2" >
				
			</div>

			<div id="div-busqueda-avanzada" style="margin-top: 15px; height: 0px; overflow: hidden;">
				<hr>
				@include('conf.busqueda-avanzada')	
			</div>

			<div class="form-group col-12 col-md-12 col-sm-12 col-lg-12 col-xl-12" style="margin-top: 20px; text-align: center;">
				<button type="button" class="btn btn-dark" id="busqueda-avanzada" data-op="abrir">B&uacute;squeda avanzada</button>
				<button type="button" class="btn btn-danger" id="btn-busqueda">Buscar</button>
			</div>

			
		</div>
	</form>
</div>